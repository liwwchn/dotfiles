#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias em="emacs"
alias ls='ls --color=auto'
PS1='[\u@\h \W]\$'

#export http_proxy='http://127.0.0.1:1080'
#export http_proxy='socks5://127.0.0.1:1080'
#export https_proxy='socks5://127.0.0.1:1080'
#export PATH=/opt/oracle/instantclient_18_3:$PATH
