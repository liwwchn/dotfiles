;;org-mode
;;(add-to-list 'load-path "~/.emacs.d/elpa/org-8.3.4/lisp/")

(require 'org)
(setq org-log-done t) 
(setq org-agenda-files (list "~/org-backup/gtd.org"
			     ;"~/org-backup/gtd.org"
			     ))
(setq org-src-fontify-natively t)
;capture
;(setq org-default-notes-file (concat org-directory "/gtd.org"))
;(add-hook 'org-mode-hook (lambda () (setq truncate-lines nil)))

;org to pdf
(setq org-latex-listings 'minted
      org-latex-packages-alist '(("" "minted"))
      org-latex-pdf-process
      '("xelatex -shell-escape -interaction nonstopmode %f"
        "xelatex -shell-escape -interaction nonstopmode %f"))
;(setq org-latex-pdf-process '("xelatex -interaction nonstopmode %f""xelatex -interaction nonstopmode %f"))
(setq org-latex-default-packages-alist
     (remove '("AUTO" "inputenc" t) org-latex-default-packages-alist))

;capture
;(setq org-default-notes-file (concat org-directory "/notes.org"))
;(add-hook 'org-mode-hook (lambda () (setq truncate-lines nil)))

;;to do list
(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "NEXT(n@)" "|" "DONE(d@)")
              (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)"))))

(setq org-todo-keyword-faces
      (quote (("TODO" :foreground "red" :weight bold)
              ("NEXT" :foreground "blue" :weight bold)
              ("DONE" :foreground "forest green" :weight bold)
              ("WAITING" :foreground "orange" :weight bold)
              ("HOLD" :foreground "magenta" :weight bold)
              ("CANCELLED" :foreground "forest green" :weight bold)
       )))

(setq org-todo-state-tags-triggers
      (quote (("CANCELLED" ("CANCELLED" . t))
              ("WAITING" ("WAITING" . t))
              ("HOLD" ("WAITING") ("HOLD" . t))
              (done ("WAITING") ("HOLD"))
              ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
              ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
              ("DONE" ("WAITING") ("CANCELLED") ("HOLD")))))

; Tags with fast selection keys
(setq org-tag-alist (quote ((:startgroup)
                            ("@HOME" . ?h)
                            ("@WORK" . ?w)
                            ("@RESEARCH" . ?r)
                            (:endgroup)
			    ("PERSONAL" . ?P)
                            ("BACKUP" . ?b))))

(setq org-agenda-custom-commands
      '(
	("w" tags-todo "+@WORK")
	("h" tags-todo "+@HOME")
        ("r" tags-todo "+@RESEARCH")
 ))


;; Capture templates 
(setq org-capture-templates
      (quote (;("t" "todo" entry (file "~/org/notes.org")"* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
	      ;("t" "todo" entry (file+olp "~/org-backup/gtd.org" "Tasks")"** TODO %?\n\%i\n" )
	      ;("t" "Todo" entry (file+headline "~/org-backup/gtd.org" "Tasks")"* TODO %? \n  %i\n  %a")
	      ("t" "Todo" entry (file+headline "~/org-backup/gtd.org" "Tasks")"* TODO %? \n  %i\n %U")
	      )))

 (setq org-agenda-custom-commands
       '(
          ("w" . "任务安排")
          ("wa" "重要且紧急的任务" tags-todo "+PRIORITY=\"A\"")
	  ("wb" "重要且不紧急的任务" tags-todo "-Weekly-Monthly-Daily+PRIORITY=\"B\"")
          ;("wb" "重要且不紧急的任务" tags-todo "+PRIORITY=\"B\"")
          ("wc" "不重要且紧急的任务" tags-todo "+PRIORITY=\"C\"")
	  ;("w" tags-todo "+@WORK")
	  ;("h" tags-todo "+@HOME")
	  ;("r" tags-todo "+@RESEARCH")
          ;("b" "Blog" tags-todo "BLOG")
          ;("p" . "项目安排")
	  ))

(provide 'init-org)
